//
//  DetailViewController.swift
//  InitWebService2
//
//  Created by Alexandre Martinez Olmos on 28/2/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var imageApp: UIImageView!
    @IBOutlet weak var titleApp: UITextField!
    @IBOutlet weak var priceApp: UITextField!
    
    var tit = ""
    var price = ""
    var img: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleApp.text = tit
        priceApp.text = price
        imageApp.image = img
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
