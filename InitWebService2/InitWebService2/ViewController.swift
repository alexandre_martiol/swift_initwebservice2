//
//  ViewController.swift
//  InitWebService2
//
//  Created by Alexandre Martinez Olmos on 27/2/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSURLConnectionDelegate, NSURLConnectionDataDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //This var will store the info of the query.
    var data = NSMutableData()
    //This var will store the data of the info.
    var tableData = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        activityIndicator.startAnimating()
        self.searchItunes()
    }
    
    func searchItunes() {
        var urlPath = "https://itunes.apple.com/search?term=Angry+Birds&media=software&limit=10"
        var url: NSURL = NSURL(string: urlPath)!
        
        //Object that capture the request of the url.
        var request: NSURLRequest = NSURLRequest(URL: url)
        
        //This object is the connection that we will use. Using "self", we say that this ViewController will receive the information.
        var connection: NSURLConnection = NSURLConnection(request:
            request, delegate: self, startImmediately: false)!
        
        println("Search iTunes API at URL \(url)")
        connection.start()
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        println("Connection failed.\(error.localizedDescription)")
    }
    
    func connection(connection: NSURLConnection, didRecieveResponse response: NSURLResponse) {
        println("Recieved response")
    }
    
    func connection(didReceiveResponse: NSURLConnection,didReceiveResponse response: NSURLResponse) {
        //Recibimos respuesta del Webservice, limpiamos el NSMutableData
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData
        data: NSData) {
        //Recibimos datos, los añadimos en el NSMutableData
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        //La petición se ha completado, tenemos los datos en data//Paso los datos a String (más fácil para trabajar)
        var dataAsString: NSString = NSString(data: self.data, encoding: NSUTF8StringEncoding)!
        
        //Convierto los datos recuperados en un objeto a través de JSON
        var err: NSError
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        
        if(jsonResult.count > 0) {
            var results: NSArray = jsonResult["results"] as! NSArray
            self.tableData = results
            
            //Aquí debes llamar al método reloadData de tu IBOutlet tabla para que vuelvan a ejecutarse los métodos numberOfRowsInSection y cellForRowAtIndexPath
            self.tableView.reloadData()
            activityIndicator.stopAnimating()
            activityIndicator.hidesWhenStopped = true
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        
        var appTitle: NSArray = tableData.valueForKey("trackName") as! NSArray
        var appPrice: NSArray = tableData.valueForKey("formattedPrice") as! NSArray
        var appImageURL: NSArray = tableData.valueForKey("artworkUrl60") as! NSArray
        
        var imgURL = NSURL(string: "\(appImageURL[indexPath.row])")!
        var imgData = NSData(contentsOfURL: imgURL)!
        
        cell.textLabel!.text = appTitle[indexPath.row] as? String
        cell.detailTextLabel!.text = appPrice[indexPath.row] as? String
        cell.imageView!.image = UIImage(data: imgData)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let detailViewController : DetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController;
        
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        
        //We have to use support vars in order to not obtain errors of optional values
        detailViewController.tit = cell!.textLabel!.text!
        detailViewController.price = cell!.detailTextLabel!.text!
        detailViewController.img = cell!.imageView!.image
        
        //load detail view controller
        self.presentViewController(detailViewController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

